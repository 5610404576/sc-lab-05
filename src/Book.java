
public class Book extends BorrowBook {
	private String authors;
	private int page;

	public Book(String nameBook,String authors,int page){
		this.nameBook = nameBook;
		this.authors = authors;
		this.page = page;
	}
	public String getauthors(){
		return authors;	
	}
	public int page(){
		return page;	
	}
}
