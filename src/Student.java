
public class Student extends LibraryUser{
	private String id;
	private String returnedDate;

	public Student(String firstname,String lastname,String id){
		this.firstname = firstname;
		this.lastname = lastname;
		this.id = id;
	}
	public String getid(){
		return id;
	}
	public String returnedDate(){
		returnedDate = "next week.";
		return returnedDate;
	}
}
