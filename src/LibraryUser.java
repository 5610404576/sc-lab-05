
public class LibraryUser {
	protected String firstname;
	protected String lastname;
	private String namebook;
	
	public String getName(){
		return firstname+"  "+lastname;
		
	}
	public String getBorrowedBook(){
		return namebook;
		
	}
	public void borrowedBook(Book book) {
		namebook = book.getnameBook();
	}
	public void returnedBook() {
		namebook = null;
	}
}
