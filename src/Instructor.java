
public class Instructor extends LibraryUser{
	private String department;
	private String returnedDate;

	public Instructor(String firstname,String lastname,String department){
		this.firstname = firstname;
		this.lastname = lastname;
		this.department = department;
	}
	public String getDepartment(){
		return department;
	}
	public String returnedDate(){
		returnedDate = "in two weeks' time.";
		return returnedDate;
	}
}
