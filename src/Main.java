
public class Main {

	public static void main(String[] args) {
		Library lib = new Library();
		Book book1 = new Book("Fifty shade of Grey", "Max", 500);
		Book book2 = new Book("ABC", "Jane", 20);
		NotlendBook unbook1 = new NotlendBook("Big java", "Tom", 680);
		NotlendBook unbook2 = new NotlendBook("Algorithm", "Edward", 550);
		Student stu = new Student("Jane", "Comsci", "5610405555");
		Instructor ins = new Instructor("Pranee", "Jaidee", "D14");
		Staff staff = new Staff("Rakna", "Dekngoh");
		
		lib.addBook(book1);
		lib.addBook(book2);
		lib.addUnborrowBook(unbook1);
		lib.addUnborrowBook(unbook2);
		
		System.out.println(lib.bookCount());
		lib.borrowBook(stu, book1);
		System.out.println(lib.getborrower());
		System.out.println("Returned date: "+stu.returnedDate());
		System.out.println(stu.getBorrowedBook());
		System.out.println(lib.bookCount());
		
		lib.borrowBook(ins, book2);
		System.out.println(lib.getborrower());
		System.out.println("Returned date: "+ins.returnedDate());
		System.out.println(ins.getBorrowedBook());
		System.out.println(lib.bookCount());
		
		System.out.println(ins.getName()+"  :"+ins.getDepartment());	
		System.out.println(staff.getName());
		System.out.println(book1.getnameBook()+", "+book1.getauthors()+", "+book1.page());
		System.out.println(book2.getnameBook()+", "+book2.getauthors()+", "+book2.page());
		System.out.println(unbook1.getnameBook()+", "+unbook1.getauthors()+", "+unbook1.page());
		System.out.println(unbook2.getnameBook()+", "+unbook2.getauthors()+", "+unbook2.page());
		
		lib.returnBook(stu,book1);
		System.out.println(lib.bookCount());
		System.out.println(stu.getBorrowedBook());
		System.out.println(lib.getborrower());
		
		lib.returnBook(ins,book2);
		System.out.println(lib.bookCount());
		System.out.println(stu.getBorrowedBook());
		System.out.println(lib.getborrower());
	}

}
