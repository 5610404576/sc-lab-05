import java.util.ArrayList;


public class Library {
	
	ArrayList<Object> store = new ArrayList<Object>();
	ArrayList<Object> unborrow = new ArrayList<Object>();
	ArrayList<String> borrower = new ArrayList<String>();
	private String bor;
	public void addBook(Book book){
		store.add(book);
	}
	public void addUnborrowBook(NotlendBook unbook){
		unborrow.add(unbook);
	}
	public int bookCount(){
		int amountBook = store.size()+unborrow.size();
		return amountBook;
	}
	public void borrowBook(LibraryUser user,Book book){
		user.borrowedBook(book);
		store.remove(book);
		borrower.add(user.getName());
	}
	public String getborrower(){
		if (borrower.size() != 0){
			bor = borrower.get(borrower.size()-1);
		}
		else {
			bor = null;
		}
		return bor;
	}
	public void returnBook(LibraryUser user,Book book){
		store.add(book);
		user.returnedBook();
		borrower.remove(user.getName());
	}
	
}
